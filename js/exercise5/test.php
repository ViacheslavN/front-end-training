<?
sleep(3);

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET)) {
        echo 'get request with params: ' . print_r($_GET, true);
    } else {
        echo 'get request without params';
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
   $rawdata = file_get_contents('php://input');
   if (!empty($rawdata)) {
        echo 'post request with data: ' . $rawdata;
   } else {
       echo 'post request without data';
   }
}
?>