/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var HttpService = exports.HttpService = function () {
    function HttpService() {
        _classCallCheck(this, HttpService);
    }

    _createClass(HttpService, null, [{
        key: "get",
        value: function get(url) {
            return new Promise(function (resolve, reject) {
                var request = new XMLHttpRequest();
                var response = void 0;
                var error = void 0;
                request.open("GET", url);
                request.onreadystatechange = function () {
                    if (request.readyState === 4 && request.status === 200) {
                        response = request.responseText;
                        resolve(response);
                    } else if (request.readyState === 4 && request.status !== 200) {
                        error = new Error(request.statusText);
                        error.code = request.status;
                        reject(error);
                    }
                };
                request.send(null);
            });
        }
    }, {
        key: "post",
        value: function post(url) {
            var body = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

            return new Promise(function (resolve, reject) {
                var request = new XMLHttpRequest();
                var response = void 0;
                var error = void 0;
                request.open("POST", url);
                request.onreadystatechange = function () {
                    if (request.readyState === 4 && request.status === 200) {
                        response = request.responseText;
                        resolve(response);
                    } else if (request.readyState === 4 && request.status !== 200) {
                        error = new Error(request.statusText);
                        error.code = request.status;
                        reject(error);
                    }
                };
                request.send(body);
            });
        }
    }]);

    return HttpService;
}();

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _HttpService = __webpack_require__(0);

var getResponse = _HttpService.HttpService.get('test.php');
getResponse.then(function (response) {
    return console.log(response);
}, function (error) {
    return console.log(error);
});

var postResponse = _HttpService.HttpService.post('test.php', 'some data');
postResponse.then(function (response) {
    return console.log(response);
}, function (error) {
    return console.log(error);
});

var errorResponse = _HttpService.HttpService.get('error.php');
errorResponse.then(function (response) {
    return console.log(response);
}, function (error) {
    return console.log(error);
});

/***/ })
/******/ ]);