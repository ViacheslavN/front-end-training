import {HttpService} from "./HttpService";

var getResponse = HttpService.get('test.php');
getResponse.then(
    response => console.log(response),
    error => console.log(error)
);

var postResponse = HttpService.post('test.php', 'some data');
postResponse.then(
    response => console.log(response),
    error => console.log(error)
);

var errorResponse = HttpService.get('error.php');
errorResponse.then(
    response => console.log(response),
    error => console.log(error)
);
