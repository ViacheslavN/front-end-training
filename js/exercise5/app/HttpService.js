'use strict';
export class HttpService {
    constructor() { }
    static get(url) {
        return new Promise((resolve, reject) => {
            let request = new XMLHttpRequest();
            let response;
            let error;
            request.open("GET", url);
            request.onreadystatechange = () => {
                if (request.readyState === 4 && request.status === 200) {
                    response = request.responseText;
                    resolve(response);
                } else if (request.readyState === 4 && request.status !== 200) {
                    error = new Error(request.statusText);
                    error.code = request.status;
                    reject(error);
                }
            };
            request.send(null);
        });
    }

    static post(url, body = null) {
        return new Promise((resolve, reject) => {
            let request = new XMLHttpRequest();
            let response;
            let error;
            request.open("POST", url);
            request.onreadystatechange = () => {
                if (request.readyState === 4 && request.status === 200) {
                    response = request.responseText;
                    resolve(response);
                } else if (request.readyState === 4 && request.status !== 200) {
                    error = new Error(request.statusText);
                    error.code = request.status;
                    reject(error);
                }
            };
            request.send(body);
        });
    }
}
