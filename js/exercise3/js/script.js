(function () {
    var actionButton = document.querySelector('.datepicker__action-button');
    actionButton.addEventListener('click', process);

    var datepicker = new Datepicker();

    function process() {
        var date = datepicker.getDate();
        if (date !== null) {
            datepicker.renderCalendar(date);
        }
    }

    function Datepicker() {
        this.dateInput = document.querySelector('.datepicker__date-input');
        this.calendarContainer = document.querySelector('.calendar');

        this.getDate = function () {
            var dateString = this.dateInput.value;

            this.dateInput.classList.remove('datepicker__date-input--invalid');

            if (this.dateIsValid(dateString)) {
                return new Date(dateString);
            } else {
                this.dateInput.classList.add('datepicker__date-input--invalid');
                return null;
            }
        }

        this.dateIsValid = function (dateString) {
            {
                // First check for the pattern
                if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
                    return false;

                // Parse the date parts to integers
                var parts = dateString.split("/");
                var day = parseInt(parts[1], 10);
                var month = parseInt(parts[0], 10);
                var year = parseInt(parts[2], 10);

                // Check the ranges of month and year
                if (year < 1000 || year > 3000 || month == 0 || month > 12)
                    return false;

                var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                // Adjust for leap years
                if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                    monthLength[1] = 29;

                // Check the range of the day
                return day > 0 && day <= monthLength[month - 1];
            };
        }

        this.renderCalendar = function (date) {
            var daysInMonth;
            var weeksInMonth;
            var firstDayInWeek;
            var arWeeksDays;
            var counter;
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var dayNames = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

            if (Object.prototype.toString.call(date) !== '[object Date]') {
                throw new Error('Invalid date type');
            }

            daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
            firstDayInWeek = new Date(date.getFullYear(), date.getMonth(), 1).getDay();

            //convert firstDayInWeek to monday base calendar
            firstDayInWeek = firstDayInWeek === 0 ? 6 : firstDayInWeek - 1;

            weeksInMonth = Math.ceil((daysInMonth + firstDayInWeek) / 7);
            arWeeksDays = new Array(weeksInMonth);
            counter = 1;
            for (var i = 0; i <= weeksInMonth - 1; i++) {
                arWeeksDays[i] = new Array(7);
                for (var j = 0; j <= 6; j++) {
                    if ((i === 0 && j < firstDayInWeek) || (counter > daysInMonth)) {
                        arWeeksDays[i][j] = null;
                    } else {
                        arWeeksDays[i][j] = counter;
                        counter++;
                    }
                }
            }

            //rendering
            while (this.calendarContainer.hasChildNodes()) {
                this.calendarContainer.removeChild(this.calendarContainer.lastChild);
            }

            var calendarTitle = document.createElement('div');
            calendarTitle.classList.add('calendar__title');

            var calendarTitleCell = document.createElement('div');
            calendarTitleCell.classList.add('calendar__cell-header');

            calendarTitleCell.innerText = monthNames[date.getMonth()] + ' ' + date.getDate();

            calendarTitle.appendChild(calendarTitleCell);

            var calendarTable = document.createElement('div');
            calendarTable.classList.add('calendar__table');

            var tableHeader = document.createElement('div');
            tableHeader.classList.add('calendar__row-header');

            for (var i = 0; i < dayNames.length; i++) {
                var cell = document.createElement('div');
                cell.classList.add('calendar__cell-header');
                cell.innerText = dayNames[i];
                tableHeader.appendChild(cell);
            }

            calendarTable.appendChild(tableHeader);


            for (var i = 0; i <= weeksInMonth - 1; i++) {
                var row = document.createElement('div');
                row.classList.add('calendar__row');
                for (var j = 0; j <= 6; j++) {
                    var cell = document.createElement('div');
                    cell.classList.add('calendar__cell');
                    if (arWeeksDays[i][j] !== null) {
                        cell.innerText = arWeeksDays[i][j];
                        if (arWeeksDays[i][j] === date.getDate()) {
                            cell.classList.add('calendar__cell--active');
                        }
                    }
                    row.appendChild(cell);
                }
                calendarTable.appendChild(row);
            }

            this.calendarContainer.appendChild(calendarTitle);
            this.calendarContainer.appendChild(calendarTable);
            this.calendarContainer.style.border = "1px solid gray";

        }
    }
})();