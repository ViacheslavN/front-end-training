(function () {
    var submitButton = document.querySelector('.form-footer__submit-btn');
    submitButton.addEventListener("click", submitAction);

    if (performance.navigation.type == 1) {
        removeFormDataFromStorage();
    }

    // window.onbeforeunload = function () {
    //     removeFormDataFromStorage();
    // }

    restoreFormData();

    function submitAction() {
        saveFormData();
        //document.querySelector('.form').submit();
    }

    function removeFormDataFromStorage() {
        if (localStorage.getItem("formData") !== null) {
            localStorage.removeItem("formData");
        }
    }

    function saveFormData() {
        var formElements = document.querySelector('.form').elements;
        var formData = {};
        for (var i = 0; i < formElements.length; i++) {
            if (formElements[i].type == "button" || formElements[i].name == "") {
                continue;
            }
            switch (formElements[i].nodeName) {
                case 'INPUT':
                    switch (formElements[i].type) {
                        case 'text':
                        case 'hidden':
                        case 'password':
                        case 'number':
                            formData[formElements[i].name] = formElements[i].value;
                            break;
                        case 'checkbox':
                        case 'radio':
                            if (formElements[i].checked) {
                                formData[formElements[i].name] = formElements[i].value;
                            }
                            break;
                    }
                    break;
                case 'TEXTAREA':
                    formData[formElements[i].name] = formElements[i].value;
                    break;

            }
        }
        localStorage.setItem("formData", JSON.stringify(formData));
    }

    function restoreFormData() {
        if (localStorage.getItem("formData") === null) {
            return;
        }

        formData = JSON.parse(localStorage.getItem("formData"));
        var formElements = document.querySelector('.form').elements;

        for (var i = 0; i < formElements.length; i++) {
            if (formElements[i].type == "button" || formElements[i].name == "") {
                continue;
            }
            switch (formElements[i].nodeName) {
                case 'INPUT':
                    switch (formElements[i].type) {
                        case 'text':
                        case 'hidden':
                        case 'password':
                        case 'number':
                            if (formElements[i].name in formData) {
                                formElements[i].value = formData[formElements[i].name]
                            }
                            break;
                        case 'checkbox':
                            if (formElements[i].name in formData) {
                                formElements[i].checked = true;
                            }
                            break;
                        case 'radio':
                            if (formElements[i].value == formData[formElements[i].name]) {
                                formElements[i].checked = true;
                            }
                            break;
                    }
                    break;
                case 'TEXTAREA':
                    if (formElements[i].name in formData) {
                        formElements[i].value = formData[formElements[i].name]
                    }
                    break;
            }
        }
    }
})();

