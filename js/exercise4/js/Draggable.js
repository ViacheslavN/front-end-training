function Draggable(selector) {
    if (typeof selector !== 'string') {
        throw new TypeError();
    }
    this.selector = selector;
    this.dropboxes = [];
    this.el = document.querySelector(selector);
    if (this.el === null) {
        return null;
    }
    this.drag();
}

Draggable.prototype.drag = function () {

    var self = this;
    var cursorOffset = {};

    var onDragStart = function (event) {
        let clientRect = self.el.getBoundingClientRect();
        cursorOffset = {
            offsetX: event.clientX - clientRect.left,
            offsetY: event.clientY - clientRect.top,
        };

        self.el.style.transitionProperty = 'none';
        self.el.style.zIndex = '1';
        self.el.style.left = event.clientX - cursorOffset.offsetX + 'px';
        self.el.style.top = event.clientY - cursorOffset.offsetY + 'px';
        document.addEventListener('mousemove', onMouseMove, false);
        document.addEventListener('mouseup', onDragStop, false);
        event.preventDefault();
    }

    var onMouseMove = function (event) {
        self.el.style.left = event.clientX - cursorOffset.offsetX + 'px';
        self.el.style.top = event.clientY - cursorOffset.offsetY + 'px';
        let clientRect = self.el.getBoundingClientRect();
        if (self.dropboxes.length > 0) {
            for (let i = 0; i < self.dropboxes.length; i++) {
                self.dropboxes[i].isHover(clientRect);
            }
        }
    }

    var onDragStop = function () {
        document.removeEventListener('mousemove', onMouseMove);
        document.removeEventListener('mouseup', onDragStop);
        self.el.style.zIndex = '0';
        let clientRect = self.el.getBoundingClientRect();
        if (self.dropboxes.length > 0) {
            for (let i = 0; i < self.dropboxes.length; i++) {
                if (self.dropboxes[i].isHover(clientRect)) {
                    self.dropboxes[i].catchElement(self);
                    break;
                }
            }
        }
    }

    self.el.addEventListener('mousedown', onDragStart, false);
}

Draggable.prototype.addDropbox = function (dropbox) {
    this.dropboxes.push(dropbox);
};





