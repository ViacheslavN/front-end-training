function Dropbox(selector) {
    if (typeof selector !== 'string') {
        throw new TypeError();
    }
    this.selector = selector;
    this.el = document.querySelector(selector);
    if (this.el === null) {
        return null;
    }
    this.clientRect = this.el.getBoundingClientRect();
    this.centerCoords = {
        x: this.clientRect.left + this.clientRect.width / 2,
        y: this.clientRect.top + this.clientRect.height / 2,
    }
}

Dropbox.prototype.isHover = function (draggableClientRect) {

    if (draggableClientRect.bottom < this.clientRect.top || draggableClientRect.top > this.clientRect.bottom || draggableClientRect.right < this.clientRect.left || draggableClientRect.left > this.clientRect.right) {
        if (this.el.style.background === 'pink') {
            this.el.style.background = 'lightgreen';
        }
        return false;
    } else {
        this.el.style.background = 'pink';
        return true;
    }

}

Dropbox.prototype.catchElement = function (draggable) {
    var self = this;

    let draggableClientRect = draggable.el.getBoundingClientRect();
    let draggableCenterOffset = {
        x: draggableClientRect.width / 2,
        y: draggableClientRect.height / 2,
    };
    let newPosition = {
        left: this.centerCoords.x - draggableCenterOffset.x,
        top: this.centerCoords.y - draggableCenterOffset.y,
    };

    var onTransitionEnd = function () {
        draggable.el.style.transitionProperty = 'none';
        self.el.style.background = 'lightgreen'
    }

    draggable.el.style.transitionProperty = 'all';
    draggable.el.style.transitionDuration = '0.5s';
    draggable.el.style.transitionTimingFunction = "linear";
    draggable.el.style.left = newPosition.left + 'px';
    draggable.el.style.top = newPosition.top + 'px';
    draggable.el.addEventListener('transitionend', onTransitionEnd, false);
};