var el1 = new Draggable('.viewport__darggable-el-1');
var el2 = new Draggable('.viewport__darggable-el-2');
var el3 = new Draggable('.viewport__darggable-el-3');

var drop1 = new Dropbox('.viewport__dropbox-1');
var drop2 = new Dropbox('.viewport__dropbox-2');
var drop3 = new Dropbox('.viewport__dropbox-3');

el1.addDropbox(drop1);
el1.addDropbox(drop2);
el1.addDropbox(drop3);

el2.addDropbox(drop1);
el2.addDropbox(drop2);
el2.addDropbox(drop3);

el3.addDropbox(drop1);
el3.addDropbox(drop2);
el3.addDropbox(drop3);